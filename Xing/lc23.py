# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def merge2Lists(self,l1,l2):
        temp = ListNode(0)
        current = temp
        while l1 and l2:
            if l1.val < l2.val:
                current.next = l1
                l1 = l1.next
            else:
                current.next = l2
                l2 = l2.next
            current = current.next
        if l1:
            current.next = l1
        if l2:
            current.next = l2
        return temp.next
    
    
    def mergeKLists(self, lists):
        """
            :type lists: List[ListNode]
            :rtype: ListNode
            """
        if lists == [] or lists == None:
            return None
        elif len(lists) == 1:
            return lists[0]
        else:
            middle = len(lists)/2
            left = self.mergeKLists(lists[:middle])
            right = self.mergeKLists(lists[middle:])
            return self.merge2Lists(left,right)
