import java.util.Stack;
/**
 * Created by ethan on 4/28/17.
 */
/*
* LC 230 Kth Smallest Element in a BST: Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.
* You may assume k is always valid, 1 ? k ? BST's total elements.
* What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? How would you optimize the kthSmallest routine?
* */
public class KthSmallestInBST {
    public int kthSmallest(TreeNode root, int k) {
        /*
        * Solution #1 In-Order Traversal: Inorder traversal of BST
        * retrieves elements of BST in sorted order. Inorder traverse
        * uses stack to store to be explred nodes of tree
        * */
        Stack<TreeNode> stack = new Stack<>();
        TreeNode n = root;
        while(n.left!=null) {
            stack.push(n);
            n = n.left;
        }

        while(k>0 && (n!=null || !stack.isEmpty())) {
            if(n==null) {
                n = stack.pop();
                if(--k==0) return n.val;
                n = n.right;
            } else {
                stack.push(n);
                n = n.left;
            }
        }
        return n.val;
    }
}
