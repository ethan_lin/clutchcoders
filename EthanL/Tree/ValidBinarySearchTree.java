/**
 * Created by ethan on 4/27/17.
 */
/*
* LC 98 Determine if a binary tree is a valid BST
* Solution # 1: Traverse
* */
public class ValidBinarySearchTree {
    private int lastVal = Integer.MIN_VALUE;
    private boolean firstNode = true;
    public boolean isValidBST(TreeNode root) {
        if (root == null) return true;

        if (!isValidBST(root.left)) return false;

        if (!firstNode && lastVal >= root.val) {
            return false;
        }
        firstNode = false;
        lastVal = root.val;

        if (!isValidBST(root.right)) return false;

        return true;
    }
}
