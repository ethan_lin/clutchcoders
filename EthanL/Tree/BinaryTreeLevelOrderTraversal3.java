import java.util.ArrayList;
import java.util.List;

/**
 * Created by ethan on 4/18/17.
 */
public class BinaryTreeLevelOrderTraversal3 {
    // Solution # 3: BFS. two queues
    public List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList result = new ArrayList();
        if (root == null) return result;

        ArrayList<TreeNode> q1 = new ArrayList<>();
        ArrayList<TreeNode> q2 = new ArrayList<>();

        q1.add(root);
        while (!q1.isEmpty()) {
            ArrayList<Integer> level = new ArrayList<>();
            q2.clear();
            for (int i = 0; i <  q1.size(); i++) {
                TreeNode node = q1.get(i);
                level.add(node.val);
                if (node.left != null) {
                    q2.add(node.left);
                }
                if (node.right != null ){
                    q2.add(node.right);
                }
            }
            ArrayList<TreeNode> temp = q1;
            q1 = q2;
            q2 = temp;
        }
        return result;
    }
}