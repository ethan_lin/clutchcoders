/**
 * Created by ethan on 4/17/17.
 */

/*
* LC 543. Diameter of Binary Tree
* Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a
* binary tree is the length of the longest path between any two nodes in a tree. This path may or may
* not pass through the root.
* */
public class DiameterOfBinaryTree {
    int max = Integer.MIN_VALUE;
    public int diameterOfBinaryTree(TreeNode root) {
        if (root == null) return 0;

        // Max Diameter through root
        max = maxDiameterViaRoot(root);
        maxDiameterHelper(root);

        return max;
    }

    private int maxDepth(TreeNode root) {
        if (root == null) return 0;
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

    private int maxDiameterViaRoot(TreeNode root) {
        if (root == null) return 0;
        else
            return maxDepth(root.left) + maxDepth(root.right);
    }

    private void maxDiameterHelper(TreeNode root) {
        // left, right subtree height
        if (root == null) return;

        max = Math.max(max, maxDiameterViaRoot(root));

        // left, right subtree diameter
        maxDiameterHelper(root.left);
        maxDiameterHelper(root.right);
    }
}
