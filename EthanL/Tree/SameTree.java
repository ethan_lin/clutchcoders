import java.util.LinkedList;

/**
 * Created by ethan on 4/17/17.
 */
public class SameTree {
    // Solution #1 : Recursive
    public  boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;
        return (p.val == q.val)
                && isSameTree(p.left, q.left)
                && isSameTree(p.right, q.right);
    }

    // Solution #2 : Iterative
    public boolean isSameTree2(TreeNode p, TreeNode q) {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;

        LinkedList<TreeNode> l1 = new LinkedList<TreeNode>();
        LinkedList<TreeNode> l2 = new LinkedList<TreeNode>();
        l1.add(p);
        l2.add(q);
        while (!l1.isEmpty() && !l2.isEmpty()) {
            TreeNode n1 = l1.poll();
            TreeNode n2 = l2.poll();

            if (n1.val != n2.val) return false;
            if ((n1.left == null && n2.left != null)
                    || (n1.left != null && n2.left == null)
                    || (n1.right == null && n2.right != null)
                    || (n1.right != null && n2.right == null))
                return false;
            if (n1.left != null && n2.left != null) {
                l1.add(n1.left);
                l2.add(n2.left);
            }

            if (n1.right != null && n2.right != null) {
                l1.add(n1.right);
                l2.add(n2.right);
            }
        }
        return true;
    }
}
