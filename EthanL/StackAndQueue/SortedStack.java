import java.util.Stack;

/*
 * Write a program to sort a stack such that the smallest items are on the top. You can use an additional temporary stack, but you may not copy the elements into any other data structure (such as an array). The stack supports the following operations: push, pop, peek, and isEmpty.
 */
public class SortedStack {
    public void sort(Stack s) {
        Stack<Integer> tempStack = new Stack<>();
        while(!s.empty()) {
            /*insert each element in sorted order*/
            int temp = (int) s.pop();
            while (!tempStack.empty() && tempStack.peek() > temp) {
                s.push(tempStack.pop());
            }
            tempStack.push(temp);
        }
        while (!tempStack.empty()) {
            s.push(tempStack.pop());
        }
    }
}
