import java.util.Stack;

/**
 * Created by demon on 2017/4/3.
 */
/*
* 150. Evaluate Reverse Polish Notation
* Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, /. Each operand may be an integer or another expression.
* */
public class EvaluateReversePolishNotation {
    public int evalRPN(String[] tokens) {
        if (tokens == null || tokens.length == 0) {
            return 0;
        }
        Stack<Integer> stack = new Stack<Integer>();
        for (String token : tokens) {
            if (token.length() > 1 || Character.isDigit(token.charAt(0)))
                stack.push(Integer.parseInt(token));
            else {
                int a = stack.pop();
                int b = stack.pop();
                if (token.equals("+")) {
                    stack.push(a + b);
                } else if (token.equals("-")) {
                    stack.push(b - a);
                } else if (token.equals("*")) {
                    stack.push(b * a);
                } else if (token.equals("/")) {
                    stack.push(b / a);
                }
            }
        }
        return stack.pop();
    }
}
