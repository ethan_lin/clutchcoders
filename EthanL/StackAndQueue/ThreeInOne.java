import java.util.EmptyStackException;
import java.util.*;

/*
 * CC189 3.1 Three in One: Describe how you could use a single array to implement three stacks
 */
public class ThreeInOne {
    // Approach 1: Fixed Division
    class FixedMultiStack {
        private int numberOfStacks = 3;
        private int stackCapacity;
        private int[] values;
        private int[] sizes;

        public FixedMultiStack(int stackSize) {
            stackCapacity = stackSize;
            values = new int[stackSize * numberOfStacks];
            sizes = new int[numberOfStacks];
        }

        // Push value onto stack
        public void push(int stackNum, int value) throws FullStackException{
            // check we have enough space for the next element
            if (isFull(stackNum)) {
                throw new FullStackException();
            }
            sizes[stackNum]++;
            values[indexOfTop(stackNum)] = value;
        }

        private boolean isFull(int stackNum) {
            return sizes[stackNum] == stackCapacity;
        }

        public int pop(int stackNum) {
            if (isEmpty(stackNum)) {
                throw new FullStackException();
            }
            int topIndex = indexOfTop(stackNum);
            int value = values[topIndex];
            values[topIndex] = 0;
            sizes[stackNum]--;
            return value;
        }
        public int peek(int stackNum) {
            if (isEmpty(stackNum)) {
                throw new EmptyStackException();
            }
            return values[indexOfTop(stackNum)];
        }

        public int indexOfTop(int stackNum) {
            int offset = stackNum * stackCapacity;
            int size = sizes[stackNum];
            return offset + size - 1;
        }

        public boolean isEmpty(int stackNum) {
            return sizes[stackNum] == 0;
        }



    }

    public class FullStackException extends RuntimeException
    {
        // no-argument constructor
        public FullStackException()
        {
            this( "Stack is full" );
        } // end no-argument FullStackException constructor

        // one-argument constructor
        public FullStackException( String exception )
        {
            super( exception );
        } // end one-argument FullStackException constructor
    } // end class FullStackException
}
