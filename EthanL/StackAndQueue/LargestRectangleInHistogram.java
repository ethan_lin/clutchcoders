import java.util.Stack;

/*
* Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, find the area of largest rectangle in the histogram.
* 84. Largest Rectangle in Histogram
* http://www.cnblogs.com/lichen782/p/leetcode_Largest_Rectangle_in_Histogram.html
* */
/**
 * Created by demon on 2017/4/4.
 */
public class LargestRectangleInHistogram {
    public int largestRectangleArea(int[] heights) {
        if (heights == null || heights.length == 0) return 0;
        //Need a stack to trace the position of the last
        Stack<Integer> stack = new Stack<>();
        int max = 0;
        for (int i = 0; i <= heights.length; i++) {
            int cur = (i == heights.length) ? -1 : heights[i];
            //stack only contains ascending numbers
            while (!stack.empty() && cur <= heights[stack.peek()]) {
                int h = heights[stack.pop()];
                int w = stack.empty() ? i : i - stack.peek() - 1;
                max = Math.max(max, h * w);
            }
            stack.push(i);
        }
        return max;
    }
}
