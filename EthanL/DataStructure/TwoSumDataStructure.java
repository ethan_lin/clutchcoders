import java.util.HashSet;
import java.util.Set;

/**
 * Created by ethan on 4/23/17.
 */
/*
* Design and implement a TwoSum class. It should support the following oprations: add and find
* add - Add numbers to an internal data structure
* find -  Find if there exists any pair of numbers which sum is equal to the value
* */
public class TwoSumDataStructure {
    Set<Integer> set = new HashSet<>();
    int currentPos = 0;
    public void add(int number) {
        set.add(number);
    }

    public boolean find(int value) {
        for (int key : set) {
            if (set.contains(value - key))
                return true;
        }
        return false;
    }
}
