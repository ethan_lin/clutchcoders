import java.util.List;

/**
 * Created by ethan on 8/8/17.
 */
public class triangle {
    public int minimumTotal(List<List<Integer>> triangle) {
        if (triangle.size() == 1) return triangle.get(0).get(0);
        int m = triangle.size();
        int[] dp = new int[m];
        // Initialize dp array with the last List of triangle\
        for (int i = 0; i < triangle.get(m - 1).size(); i++) {
            dp[i] = triangle.get(m - 1).get(i);
        }
        for (int i = m - 2; i >= 0; i--) {
            for (int j = 0; j < triangle.get(i).size(); j++) {
                dp[j] = Math.min(dp[j], dp[j + 1]) + triangle.get(i).get(j);
            }
        }
        return dp[0];
    }
}
