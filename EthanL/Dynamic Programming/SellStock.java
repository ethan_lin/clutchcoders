/*
 * Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
 */
public class SellStock {
    public int maxProfit(int[] prices) {

        if (prices == null || prices.length < 2) return 0;
        int result = 0;
        int minP = prices[0];
        for (int i = 1; i < prices.length; i++) {
            result = Math.max(result, prices[i] - minP);
            minP = Math.min(minP, prices[i]);
        }
        return result;
    }
}
