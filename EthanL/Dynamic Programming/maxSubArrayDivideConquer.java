/**
 * Created by ethan on 8/5/17.
 */
public class maxSubArrayDivideConquer {
    public int maxSubArray(int[] nums) {
        int maxV = Integer.MIN_VALUE;
        return maxArray(nums,0, nums.length - 1, maxV);
    }

    private int maxArray(int[] nums, int start, int end, int maxV) {
        if (start > end) return Integer.MIN_VALUE;
        int mid = (start + (end - start) / 2);
        int startMax = maxArray(nums, start, mid - 1, maxV);
        int endMax = maxArray(nums, mid + 1, end, maxV);
        maxV = Math.max(maxV, startMax);
        maxV = Math.max(maxV, endMax);
        int sum = 0, mlmax = 0;
        for (int i = mid - 1; i >= start; i--) {
            sum += nums[i];
            if (sum > mlmax) {
                mlmax = sum;
            }
        }
        sum = 0;
        int mrmax = 0;
        for (int i = mid + 1; i <= end; i++) {
            sum += nums[i];
            if (sum > mrmax)
                mrmax = sum;
        }
        maxV = Math.max(maxV, mlmax + mrmax + nums[mid]);
        return maxV;
    }
}
