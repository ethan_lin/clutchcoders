import java.util.ArrayList;

/*
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
 * Given nums = [-2, 0, 3, -5, 2, -1]
sumRange(0, 2) -> 1
sumRange(2, 5) -> -1
sumRange(0, 5) -> -3
 */
public class RangeSumQuery {
    int[] dp;

    public RangeSumQuery(int[] nums) {
        dp = new int[nums.length];
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            dp[i] = sum;
        }
    }
    public int sumRange(int i, int j) {
        return i == 0 ? dp[0] : dp[j] - dp[i - 1];
    }
}
