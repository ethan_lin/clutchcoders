/*
 * CC189 2.4 Partition a linked list around a value x, such that all nodes less than x
  * come before all nodes greater than or equal to x. If x is contained within the list
  * the values of x only need to be after the elements less than x. The partition element
  * can appear anywhere in the "right partition", it does not need to appear between the left
  * and right partition
 */
public class Partition {
    // Create two linked list, one for elements less than x, and one for elements greater
    // than or equal to x
    ListNode partition(ListNode node, int x) {
        ListNode beforeStart = null;
        ListNode beforeEnd = null;
        ListNode afterStart = null;
        ListNode afterEnd = null;
        while (node != null) {
            ListNode next = node.next;
            node.next = null;
            if (node.val < x) {
                if (beforeStart == null) {
                    beforeStart = node;
                    beforeEnd = beforeStart;
                } else {
                    beforeEnd.next = node;
                    beforeEnd = node;
                }
            } else {
                if (afterStart == null) {
                    afterStart = node;
                    afterEnd = afterStart;
                } else {
                    afterEnd.next = node;
                    afterEnd = node;
                }
            }
            node = next;
        }
        if (beforeStart == null) {
            return afterStart;
        }
        beforeEnd.next = afterStart;
        return beforeStart;
    }
}
