/*
 * LeetCode 61 Rotate List: Given a list, rotate the list to the right by k places, where k is non-negative.
 * When it comes to ListNode questions, recursion, two pointers, and link head and tail are always common ways to considered about
 *
 */
public class RotateList {
    public ListNode rotateRight(ListNode head, int k) {
        if (head == null || k == 0) return head;

        int len = 1;
        ListNode last = head;

        while (last.next != null) {
            last = last.next;
            len++;
        }

        last.next = head;
        int i = len - k % len;
        ListNode pre = last;

        while (i > 0) {
            pre = pre.next;
            i--;
        }

        head = pre.next;
        pre.next = null;
        return head;
    }
}
