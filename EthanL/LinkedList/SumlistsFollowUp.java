/*
 * Suppose digits are stored in forward order. Repeat the above problem.
 */
public class SumlistsFollowUp {
     // Since the result is going to added to the head, the recursive call need to
     // report to the head
    class PartialSum {
         public ListNode sum = null;
         public int carry = 0;
    }
    ListNode addLists(ListNode n1, ListNode n2) {
        int len1 = length(n1);
        int len2 = length(n2);

        if (len1 < len2) {
            n1 = padList(n1, len2 - len1);
        } else {
            n2 = padList(n2, len1 - len2);
        }
        // Add lists
        PartialSum sum = addListsHelper(n1, n2);

        // if there was a carry value left over, insert this at the front of the list
        // otherwise, just return the linked list
        if (sum.carry == 0) {
            return sum.sum;
        } else {
            ListNode result = insertBefore(sum.sum, sum.carry);
            return result;
        }
    }

    private ListNode insertBefore(ListNode head, int carry) {
        ListNode before = new ListNode(carry);
        if (head != null)
            before.next = head;
        return before;
    }

    public PartialSum addListsHelper(ListNode n1, ListNode n2) {
        if (n1 == null && n2 == null) {
            PartialSum sum = new PartialSum();
            return sum;
        }
        // Add smaller digits recursively
        PartialSum sum = addListsHelper(n1.next, n2.next);

        // Add carry to current data
        int val = sum.carry + n1.val + n2.val;

        //Insert Sum of current digits
        ListNode full_result = insertBefore(sum.sum, val % 10);

        // Return sum so far and carry value
        sum.sum = full_result;
        sum.carry = val / 10;
        return sum;
    }

    public ListNode padList(ListNode n1, int padding) {
        ListNode head = n1;
        for (int i = 0; i < padding; i++) {
            head = insertBefore(head, 0);
        }
        return head;
    }

    public int length(ListNode head) {
        if (head == null) return 0;
        int counter = 0;
        while (head != null) {
            counter++;
            head = head.next;
        }
        return counter;
    }
}
