import java.util.Stack;

/*
 * CC189 2.6 Palindrome Second Solution Iterations
 * Main idea behind this is to use stack to store the first half of the list
 * if we dont know the size, we could use the fast and slow runner to check and
 * push slow runner into the stack
 */
public class Palindrome2 {
    public boolean isPalindrome2(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        Stack<Integer> stack = new Stack<>();
        while(fast != null && fast.next != null) {
            stack.push(slow.val);
            slow = slow.next;
            fast = fast.next.next;
        }

        // For odd number of elements, so skip the middle element
        if (fast != null) {
            slow = slow.next;
        }
        while (slow != null) {
            int top = stack.pop().intValue();

            // If the values are different, then it is not a palindrome
            if (top != slow.val) {
                return false;
            }
            slow = slow.next;
        }
        return true;
    }
}
