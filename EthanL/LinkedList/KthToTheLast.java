/**
 * Created by demon on 2017/4/5.
 */
public class KthToTheLast {
    // Recursive Solution
    public int printKthToLast(ListNode head, int k) {
        if (head == null) {
            return 0;
        }
        int index = printKthToLast(head.next, k) + 1;
        if (index == k) {
            System.out.println(k + "th to last node is" + head.val);
        }
        return index;
    }

    // Another Recursive Solution Using Wrapper Class
    /*
    * we couldn't simultaneously return a counter and an index. If
    * we wrap the counter value with simple class (or even a single element array),
    * we can mimic passing by reference.*/
    class Index {
        public int value = 0;
    }

    public ListNode kthToLast(ListNode head, int k) {
        Index index = new Index();
        return kthToLast(head, k, index);
    }

    public ListNode kthToLast(ListNode head, int k, Index index) {
        if (head == null) {
            return null;
        }
        ListNode node = kthToLast(head.next, k, index);
        index.value = index.value + 1;
        if (index.value == k) {
            return head;
        }
        return node;
    }

    //Iterative Solution
    /*
    * we put two pointers k nodes apart and iterate until last node hit the end
    * */
    ListNode nthTolast(ListNode head, int k) {
        ListNode n1 = head;
        ListNode n2 = head;
        // Move p2 k nodes into the list
        for (int i = 0; i <  k; i++) {
            if(n2 == null) return null;
            n2 = n2.next;
        }
        while(n2 != null) {
            n1 = n1.next;
            n2 = n2.next;
        }

        return n1;
    }
}
