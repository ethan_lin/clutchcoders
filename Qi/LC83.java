// 83. Remove Duplicates from Sorted List
// Given a sorted linked list, delete all duplicates such that each element appear only once.

// For example,
// Given 1->1->2, return 1->2.
// Given 1->1->2->3->3, return 1->2->3.

public class LC83{
	public ListNode deleteDuplicates(ListNode head) { // using a set, bad solution
        if(head == null){
            return null;
        }
        HashSet<Integer> set = new HashSet<>();
        ListNode curNode = head;
        set.add(curNode.val);
        while(curNode.next!=null){
            if(set.contains(curNode.next.val)){
                if(curNode.next.next!=null){
                    curNode.next = curNode.next.next;
                }else{
                    curNode.next = null;
                    return head;
                }
                // curNode = curNode.next; // should be curNode = curNode;
            }else{
                set.add(curNode.next.val);
                curNode = curNode.next;
            }
        }
        return head;
    }

    public ListNode deleteDuplicates2(ListNode head) { // using a set, bad solution
        if(head == null){
            return null;
        }
        HashSet<Integer> set = new HashSet<>();
        ListNode curNode = head;
        set.add(curNode.val);
        while(curNode.next!=null){
            if(set.contains(curNode.next.val)){ //compared to solution 1 this part is more concise
                curNode.next = curNode.next.next;
                // curNode = curNode.next; // should be curNode = curNode;
            }else{
                set.add(curNode.next.val);
                curNode = curNode.next;
            }
        }
        return head;
    }

    public ListNode deleteDuplicates3(ListNode head) { // since list are sorted, so duplicates must be neighbors
        if(head == null || head.next == null){
            return head;
        }
        ListNode curNode = head;
        while(curNode.next!=null){
            if(curNode.val == curNode.next.val){
                curNode.next = curNode.next.next;
            }else{
                curNode = curNode.next;
            }
        }
        return head;
    }
}