//349. Intersection of Two Arrays
// Given two arrays, write a function to compute their intersection.

// Example:
// Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2].

// Note:
// Each element in the result must be unique.
// The result can be in any order.

import java.util.*;

public class LC349{
	//time complexity o(n)
	//the reason o(n) solution works is that the result can be in any order, so we don't have to do sort.
	public int[] intersection(int[] nums1, int[] nums2){
        Set<Integer> set1 = new HashSet<Integer>();
        for(int num: nums1){ // put the unique nums from first array to hashset1
            if(!set1.contains(num)){
                set1.add(num);
            }
        }
        Set<Integer> set2 = new HashSet<Integer>();
        for(int num: nums2){ // put the intersection to hashset2
            if(set1.contains(num)){
                set2.add(num);
            }
        }
        
        int[] ans = new int[set2.size()];
        int i = 0;
        
        for(int num: set2){
            ans[i++] = num;
        }
        return ans;
	}

	//time complexity o(nlogn), because of the pre-sorting
	public int[] intersection2(int[] nums1, int[] nums2){
		Set<Integer> set = new HashSet<Integer>();
		Arrays.sort(nums1); // sorting takes o(nlogn)
		Arrays.sort(nums2);
		int i = 0; 
		int j = 0;

		while( i < nums1.length && j < nums2.length){ // two pointer approach takes o(n)
			if(nums1[i] < nums2[j]){
				i ++;
			}else if(nums1[i] > nums2[j]){
				j ++;
			}else{
				set.add(nums1[i]);
				i++;
				j++;
			}
		}

		int[] res = new int[set.size()];
		int k = 0;
		for(int num: set){
			res[k++] = num;
		}
		return res;
	}


	public static void main(String [] args){
		int[] nums1 = {1,2,2,1};
		int[] nums2 = {2, 2};
		LC349 l = new LC349();
		int[] res = l.intersection2(nums1, nums2);
		System.out.println(Arrays.toString(res));
	}
}