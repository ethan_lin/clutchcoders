package LinkedList;

/**
 * Created by Nate He on 4/8/2017.
 * how to find mid of linkedLIst and how to reverse LinkedList iteratively
 */
public class LC234PalindromeLinkedList {
    public boolean isPalindrome(ListNode head) {
        if(head == null || head.next == null){
            return true;
        }
        ListNode firstHalf = head;
        ListNode slow = head;
        ListNode fast = head;
        while(fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode secondHalf = slow.next;
        slow.next = null;

        secondHalf = reverseList(secondHalf);
        while( secondHalf != null){
            if( firstHalf.val != secondHalf.val){
                return false;

            }else{
                firstHalf = firstHalf.next;
                secondHalf = secondHalf.next;
            }
        }
        return true;

    }
    public ListNode reverseList(ListNode head){
        if(head == null || head.next == null){
            return head;
        }
        ListNode pre = null;
        ListNode curr = head;

        while( curr != null){
            ListNode next = curr.next;
            curr.next = pre;
            pre = curr;
            curr = next;
        }
        return pre;
    }
}
