package LinkedList;

/**
 * Created by Nate He on 3/30/2017.
 */
public class LC206ReverseLinkedList {
    // recursively
    public ListNode reverseList(ListNode head) {
       if(head == null || head.next == null){
           return head;
       }
       ListNode p = reverseList(head.next);
       head.next.next = head;
       head.next = null;
       return p;
    }
    // interatively reverse list;

    public ListNode reveseList2(ListNode head){
        if( head == null || head.next == null){
            return head;
        }
        ListNode curr = head;
        ListNode reversePart = null;
        while( curr != null){
            ListNode next = curr.next;
            curr.next = reversePart;
            reversePart = curr;
            curr = next;
        }
        return reversePart;
    }
}
