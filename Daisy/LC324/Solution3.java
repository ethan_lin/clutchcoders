package LC324;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution3 {
    // can not work good with duplicate cases
    // time complexity : O(N), space complexity: O(N)
    public void wiggleSort(int[] nums) {
        int median = selectKth(nums, 0, nums.length - 1, nums.length % 2 == 0 ? nums.length / 2 : nums.length / 2 + 1);
        List<Integer> leftArr = new ArrayList<Integer>();
        for (int i = 0; i <= median; i++)
            leftArr.add(nums[i]);
        List<Integer> rightArr = new ArrayList<Integer>();
        for (int i = median + 1; i < nums.length; i++)
            rightArr.add(nums[i]);
        for (int li = leftArr.size() - 1, ri = rightArr.size() - 1, i = 0; ri >= 0; li--, ri--, i += 2) { // right is same or shorter than left
            nums[i] = leftArr.get(li);
            nums[i + 1] = rightArr.get(ri);
        }
        if (nums.length % 2 != 0)
            nums[nums.length - 1] = leftArr.get(0);
    }

    private int selectKth(int[] nums, int start, int end, int k) {
        int m = partition(nums, start, end);
        if (m + 1 < k) {
            return selectKth(nums, m + 1, end, k);
        } else if (m + 1 > k) {
            return selectKth(nums, start, m - 1, k);
        } else {
            // return index, not value
            return m;
        }
    }

    private int partition(int[] nums, int start, int end) {
        int pos = nums[start];
        while (start < end) {
            while (start < end && nums[end] >= pos) {
                end--;
            }
            nums[start] = nums[end];
            while (start < end && nums[start] <= pos) {
                start++;
            }
            nums[end] = nums[start];
        }
        nums[start] = pos;
        return start;
    }
}
