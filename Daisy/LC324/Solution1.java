package LC324;

import java.util.Arrays;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution1 {
    public static void wiggleSort(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }

        Arrays.sort(nums);

        int[] tmp = new int[nums.length];
        int k = 0;
        for (int i = (nums.length - 1)/ 2, j = nums.length - 1; i >= 0; i--, j--) {
            tmp[k++] = nums[i];
            if(k < nums.length) {
                tmp[k++] = nums[j];
            }
        }

        // need copy each element, not just nums = tmp
        for (int i = 0; i < nums.length; i++) {
            nums[i] = tmp[i];
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,5,1,1,6,4};
        wiggleSort(nums);

        for(int n : nums){
            System.out.println(n + " ");
        }
    }
}
