package LC78;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution1 {
    // time complexity : O(n^2)
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        helper(nums, ans, list, 0);
        return ans;
    }

    private void helper(int[] nums, List<List<Integer>> ans, List<Integer> list, int pos) {
        if (pos <= nums.length) {
            ans.add(new ArrayList<Integer>(list));
        }

        for (int i = pos; i < nums.length; i++) {
            list.add(nums[i]);
            helper(nums, ans, list, i + 1);
            list.remove(list.size() - 1);
        }
    }
}
