package LC78;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution3 {
    // time complexity : O(N ^ 2)
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        ans.add(list);
        for (int i = 0; i < nums.length; i++) {
            // calculate size first
            int size = ans.size();
            for (int j = 0; j < size; j++) {
                // new a copy of ans.get(j)
                list = new ArrayList<Integer>(ans.get(j));
                list.add(nums[i]);
                ans.add(list);
            }
        }
        return ans;
    }
}
