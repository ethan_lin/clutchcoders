package LC56;

import java.util.*;

/**
 * Created by lipingzhang on 4/6/17.
 */
class Interval {
    int start;
    int end;
    Interval() { start = 0; end = 0; }
    Interval(int s, int e) { start = s; end = e; }
}
public class Solution1 {
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> ans = new ArrayList<>();
        if (intervals == null || intervals.size() == 0) {
            return ans;
        }

        // Arrays.sort doesn't work
        Collections.sort(intervals, (x, y) -> (x.start - y.start));

        /*
        Collections.sort(intervals, new Comparator<Interval>() {
            public int compare(Interval x, Interval y) {
                return x.start - y.start;
            }
        });
        */
        Interval inter = intervals.get(0);
        for (int i = 1; i < intervals.size(); i++) {
            Interval next = intervals.get(i);
            if (next.start <= inter.end) {
                inter.start = Math.min(inter.start, next.start);
                inter.end = Math.max(inter.end, next.end);
            } else {
                // new Interval, not just add
                ans.add(new Interval(inter.start, inter.end));
                inter.start = next.start;
                inter.end = next.end;
            }
        }

        ans.add(inter);
        return ans;
    }
}
