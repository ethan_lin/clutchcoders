/**
 * Easy - Remove Linked List Elements
 * 
 * Remove all elements from a linked list of integers that have value val.
 * Example
 * Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
 * Return: 1 --> 2 --> 3 --> 4 --> 5
 * @author zy
 *
 */
public class LC203 {
	
	public ListNode removeElements(ListNode head, int val) {
        if (head == null){
        		return null;
        }
        while (head.val == val && head.next!= null){
        		head = head.next;
        }
        if (head.next == null){
        		if (head.val == val){
        			return null;
        		}
        		else{
        			return head;
        		}
        }
        
        ListNode keep = new ListNode(head.val);
        ListNode pointer = keep;
        ListNode node = head.next;
        while (node!= null){
        		if (node.val != val){
        			ListNode good = new ListNode(node.val);
        			pointer.next = good;
        			pointer = good;
        		}
        		node = node.next;
        }
        return keep;     
    }
}
